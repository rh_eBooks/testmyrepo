FasdUAS 1.101.10   ��   ��    k             p         �� �� 0 
rootfolder 
rootFolder  �� 	�� 0 
hasfolders 
hasFolders 	 �� 
�� 0 	alltitles 	allTitles 
 �� �� 0 successcount successCount  �� �� 0 problemcount problemCount  �� �� 0 thefilename theFileName  �� �� 0 theepub theEpub  �� �� 0 	mainloopx 	mainLoopX  �� �� 0 everythingcool    �� �� 0 
theproblem 
theProblem  �� �� 0 theepubname theEpubName  �� �� 0 	error_log    �� �� 0 dragdrop    �� �� 0 theitems theItems  �� �� 0 thisitem    �� �� 0 isownfolders isOwnFolders  �� �� "0 pathtoepubcheck pathToEpubcheck  ������ 0 theepubfolder theEpubFolder��        l     ��������  ��  ��        j     �� �� 20 currentepubcheckversion currentEpubcheckVersion  m        �   
 4 . 0 . 2     !   l     ��������  ��  ��   !  " # " l     �� $ %��   $  Release notes for 1.2    % � & & * R e l e a s e   n o t e s   f o r   1 . 2 #  ' ( ' l     ��������  ��  ��   (  ) * ) l     �� + ,��   + t nupdated to run IDPF's epubcheck v1.2, which has some better error reporting but few other significant changes.    , � - - � u p d a t e d   t o   r u n   I D P F ' s   e p u b c h e c k   v 1 . 2 ,   w h i c h   h a s   s o m e   b e t t e r   e r r o r   r e p o r t i n g   b u t   f e w   o t h e r   s i g n i f i c a n t   c h a n g e s . *  . / . l     ��������  ��  ��   /  0 1 0 l     ��������  ��  ��   1  2 3 2 l     �� 4 5��   4   Release notes for v2.0    5 � 6 6 .   R e l e a s e   n o t e s   f o r   v 2 . 0 3  7 8 7 l     ��������  ��  ��   8  9 : 9 l     �� ; <��   ; | v All neccesary resources are now bundled within the application itself, no need to download or point to any .jar files    < � = = �   A l l   n e c c e s a r y   r e s o u r c e s   a r e   n o w   b u n d l e d   w i t h i n   t h e   a p p l i c a t i o n   i t s e l f ,   n o   n e e d   t o   d o w n l o a d   o r   p o i n t   t o   a n y   . j a r   f i l e s :  > ? > l     �� @ A��   @   Make drag and drop.    A � B B (   M a k e   d r a g   a n d   d r o p . ?  C D C l     ��������  ��  ��   D  E F E l      �� G H��   G � �
Notes for v 3.0

- Updated ePubCheck jar to epubcheck v3.0, released 2012-12-21, here: http://code.google.com/p/epubcheck/
- Removed obsolete Preflight and references

TODO
allow dragging of epub files, not just folders
    H � I I� 
 N o t e s   f o r   v   3 . 0 
 
 -   U p d a t e d   e P u b C h e c k   j a r   t o   e p u b c h e c k   v 3 . 0 ,   r e l e a s e d   2 0 1 2 - 1 2 - 2 1 ,   h e r e :   h t t p : / / c o d e . g o o g l e . c o m / p / e p u b c h e c k / 
 -   R e m o v e d   o b s o l e t e   P r e f l i g h t   a n d   r e f e r e n c e s 
 
 T O D O 
 a l l o w   d r a g g i n g   o f   e p u b   f i l e s ,   n o t   j u s t   f o l d e r s 
 F  J K J l     ��������  ��  ��   K  L M L l     �� N O��   N  Release notes for v4.0.1    O � P P 0 R e l e a s e   n o t e s   f o r   v 4 . 0 . 1 M  Q R Q l     ��������  ��  ��   R  S T S l     �� U V��   U � �updated to run epubcheck v4.0.1. Error reporting has been updated to include error/warning codes. Error reporting messages have slightly changed. Updated primarily for eduPub    V � W W\ u p d a t e d   t o   r u n   e p u b c h e c k   v 4 . 0 . 1 .   E r r o r   r e p o r t i n g   h a s   b e e n   u p d a t e d   t o   i n c l u d e   e r r o r / w a r n i n g   c o d e s .   E r r o r   r e p o r t i n g   m e s s a g e s   h a v e   s l i g h t l y   c h a n g e d .   U p d a t e d   p r i m a r i l y   f o r   e d u P u b T  X Y X l     ��������  ��  ��   Y  Z [ Z i     \ ] \ I     �� ^��
�� .aevtodocnull  �    alis ^ o      ���� 0 theitems theItems��   ] k      _ _  ` a ` r      b c b o     ���� 0 theitems theItems c o      ���� 0 thisitem   a  d e d r     f g f m    ��
�� boovtrue g o      ���� 0 dragdrop   e  h�� h I    �������� 0 main  ��  ��  ��   [  i j i l     ��������  ��  ��   j  k l k i    
 m n m I     ������
�� .aevtoappnull  �   � ****��  ��   n k     	 o o  p q p r      r s r m     ��
�� boovfals s o      ���� 0 dragdrop   q  t�� t I    	�������� 0 main  ��  ��  ��   l  u v u l     ��������  ��  ��   v  w x w i     y z y I      �������� 0 main  ��  ��   z k     ~ { {  | } | l     �� ~ ��   ~ � �set pathToEpubcheck to POSIX path of file ((path to me as text) & "Contents:Resources:epubcheck-" & currentEpubcheckVersion & ".jar")     � � �
 s e t   p a t h T o E p u b c h e c k   t o   P O S I X   p a t h   o f   f i l e   ( ( p a t h   t o   m e   a s   t e x t )   &   " C o n t e n t s : R e s o u r c e s : e p u b c h e c k - "   &   c u r r e n t E p u b c h e c k V e r s i o n   &   " . j a r " ) }  � � � r      � � � l     ����� � c      � � � m      � � � � � J ~ / C o m m o n T o o l s / e p u b c h e c k / e p u b c h e c k . j a r � m    ��
�� 
ctxt��  ��   � o      ���� "0 pathtoepubcheck pathToEpubcheck �  � � � l   ��������  ��  ��   �  � � � r    	 � � � m    ����   � o      ���� 0 successcount successCount �  � � � r   
  � � � m   
 ����   � o      ���� 0 problemcount problemCount �  � � � l   ��������  ��  ��   �  � � � I    �������� 0 
getfolders 
getFolders��  ��   �  � � � l   ��������  ��  ��   �  � � � r     � � � m    ����  � o      ���� 0 	mainloopx 	mainLoopX �  � � � W    x � � � k   $ s � �  � � � r   $ ' � � � m   $ %��
�� boovtrue � o      ���� 0 everythingcool   �  � � � I   ( -�������� 0 getepub getEpub��  ��   �  � � � Z  . = � ����� � =  . 1 � � � o   . /���� 0 everythingcool   � m   / 0��
�� boovtrue � I   4 9�������� 0 epubcheckit ePubCheckIt��  ��  ��  ��   �  � � � l  > >�� � ���   � 0 *if everythingcool is true then PFCheckIt()    � � � � T i f   e v e r y t h i n g c o o l   i s   t r u e   t h e n   P F C h e c k I t ( ) �  � � � Z  > M � ����� � =  > A � � � o   > ?���� 0 everythingcool   � m   ? @��
�� boovfals � I   D I��~�}� 0 errorhandler errorHandler�~  �}  ��  ��   �  � � � r   N S � � � [   N Q � � � o   N O�|�| 0 	mainloopx 	mainLoopX � m   O P�{�{  � o      �z�z 0 	mainloopx 	mainLoopX �  � � � Z  T c � ��y�x � =  T W � � � o   T U�w�w 0 everythingcool   � m   U V�v
�v boovtrue � r   Z _ � � � [   Z ] � � � o   Z [�u�u 0 successcount successCount � m   [ \�t�t  � o      �s�s 0 successcount successCount�y  �x   �  ��r � Z  d s � ��q�p � =  d g � � � o   d e�o�o 0 everythingcool   � m   e f�n
�n boovfals � r   j o � � � [   j m � � � o   j k�m�m 0 problemcount problemCount � m   k l�l�l  � o      �k�k 0 problemcount problemCount�q  �p  �r   � ?    # � � � o    �j�j 0 	mainloopx 	mainLoopX � l   " ��i�h � I   "�g ��f
�g .corecnte****       **** � o    �e�e 0 	alltitles 	allTitles�f  �i  �h   �  ��d � I   y ~�c�b�a�c 0 fanfare  �b  �a  �d   x  � � � l     �`�_�^�`  �_  �^   �  � � � i     � � � I      �]�\�[�] 0 
getfolders 
getFolders�\  �[   � l    � � � � � O     � � � � k    � � �  � � � I   	�Z�Y�X
�Z .miscactvnull��� ��� obj �Y  �X   �  � � � r   
  � � � m   
 �W
�W boovfals � o      �V�V 0 foldertested   �  � � � W    � � � � k    � � �  � � � l   �U � ��U   �   get the folder from user    � � � � 2   g e t   t h e   f o l d e r   f r o m   u s e r �  � � � Z    - � ��T � � =    � � � o    �S�S 0 dragdrop   � m    �R
�R boovfals � r    % � � � I   #�Q�P �
�Q .sysostflalis    ��� null�P   � �O ��N
�O 
prmp � m     � � �   b P l e a s e   s e l e c t   a   f o l d e r   c o n t a i n i n g   e p u b s   t o   c h e c k .�N   � o      �M�M 0 
rootfolder 
rootFolder�T   � r   ( - c   ( + o   ( )�L�L 0 thisitem   m   ) *�K
�K 
alis o      �J�J 0 
rootfolder 
rootFolder �  l  . .�I�I   ) # determine if it has folders or not    �		 F   d e t e r m i n e   i f   i t   h a s   f o l d e r s   o r   n o t 

 r   . ? 6  . = n   . 4 2   2 4�H
�H 
file 4   . 2�G
�G 
cfol o   0 1�F�F 0 
rootfolder 
rootFolder D   5 < 1   6 8�E
�E 
pnam m   9 ; � 
 . e p u b o      �D�D 0 folder_test_1    l  @ @�C�B�A�C  �B  �A    Z   @ [�@�? >  @ G l  @ E�>�= I  @ E�< �;
�< .corecnte****       ****  o   @ A�:�: 0 folder_test_1  �;  �>  �=   m   E F�9�9   l  J W!"#! k   J W$$ %&% r   J M'(' m   J K�8
�8 boovfals( o      �7�7 0 
hasfolders 
hasFolders& )*) r   N S+,+ o   N O�6�6 0 folder_test_1  , o      �5�5 0 	alltitles 	allTitles* -�4- r   T W./. m   T U�3
�3 boovtrue/ o      �2�2 0 foldertested  �4  "   it's loose epubs   # �00 "   i t ' s   l o o s e   e p u b s�@  �?   121 l  \ \�1�0�/�1  �0  �/  2 3�.3 Z   \ �45�-�,4 =  \ c676 l  \ a8�+�*8 I  \ a�)9�(
�) .corecnte****       ****9 o   \ ]�'�' 0 folder_test_1  �(  �+  �*  7 m   a b�&�&  5 l  f �:;<: k   f �== >?> r   f v@A@ 6  f tBCB n   f iDED 2   g i�%
�% 
cfolE o   f g�$�$ 0 
rootfolder 
rootFolderC C  j sFGF 1   k m�#
�# 
pnamG m   n rHH �II  9 7 8A o      �"�" 0 folder_test_2  ? J�!J Z   w �KL� MK >  w ~NON l  w |P��P I  w |�Q�
� .corecnte****       ****Q o   w x�� 0 folder_test_2  �  �  �  O m   | }��  L k   � �RR STS r   � �UVU m   � ��
� boovtrueV o      �� 0 
hasfolders 
hasFoldersT WXW r   � �YZY o   � ��� 0 folder_test_2  Z o      �� 0 	alltitles 	allTitlesX [�[ r   � �\]\ m   � ��
� boovtrue] o      �� 0 foldertested  �  �   M k   � �^^ _`_ I  � ��ab
� .sysodlogaskr        TEXTa m   � �cc �dd � P l e a s e   s e l e c t   a   f o l d e r   t h a t   c o n t a i n s   e i t h e r   . e p u b   f i l e s ,   o r   I S B N   f o l d e r s .b �e�
� 
dispe m   � ���  �  ` f�f l  � ��gh�  g  	try again   h �ii  t r y   a g a i n�  �!  ;   it's ISBN folders   < �jj $   i t ' s   I S B N   f o l d e r s�-  �,  �.   � =   klk o    �� 0 foldertested  l m    �
� boovtrue � m�
m l  � ��	���	  �  �  �
   � m     nn�                                                                                  MACS  alis    �  32132_TBozanicMBP          �06H+   �!
Finder.app                                                      w��6@%        ����  	                CoreServices    �0Y�      �6xe     �!   !     ;32132_TBozanicMBP:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p  $  3 2 1 3 2 _ T B o z a n i c M B P  &System/Library/CoreServices/Finder.app  / ��   � h b gets a folder, sets hasFolders to true or false, depending on whether it has loose epubs or isbns    � �oo �   g e t s   a   f o l d e r ,   s e t s   h a s F o l d e r s   t o   t r u e   o r   f a l s e ,   d e p e n d i n g   o n   w h e t h e r   i t   h a s   l o o s e   e p u b s   o r   i s b n s � pqp l     ����  �  �  q rsr i    tut I      ���� 0 getepub getEpub�  �  u O     �vwv k    �xx yzy Z    {|� ��{ =   }~} o    ���� 0 
hasfolders 
hasFolders~ m    ��
�� boovfals| k   
  ��� r   
 ��� n   
 ��� 4    ���
�� 
cobj� o    ���� 0 	mainloopx 	mainLoopX� o   
 ���� 0 	alltitles 	allTitles� o      ���� 0 theepub theEpub� ���� r    ��� m    �� ���  e p u b� o      ���� 0 	filefound 	fileFound��  �   ��  z ��� Z    �������� =   ��� o    ���� 0 
hasfolders 
hasFolders� m    ��
�� boovtrue� k    ��� ��� r    '��� n    %��� 1   # %��
�� 
pnam� n    #��� 4     #���
�� 
cobj� o   ! "���� 0 	mainloopx 	mainLoopX� o     ���� 0 	alltitles 	allTitles� o      ����  0 thisfoldername thisFolderName� ��� l  ( (������  � # display dialog thisFolderName   � ��� : d i s p l a y   d i a l o g   t h i s F o l d e r N a m e� ��� r   ( 1��� n   ( /��� 4   , /���
�� 
cfol� m   - .�� ��� $ P r o d u c t : e B o o k : e P u b� n   ( ,��� 4   ) ,���
�� 
cobj� o   * +���� 0 	mainloopx 	mainLoopX� o   ( )���� 0 	alltitles 	allTitles� o      ���� 0 theepubfolder theEpubFolder� ��� r   2 @��� 6  2 >��� n   2 5��� 2  3 5��
�� 
file� o   2 3���� 0 theepubfolder theEpubFolder� D   6 =��� 1   7 9��
�� 
pnam� m   : <�� ��� 
 . e p u b� o      ���� 0 theepub theEpub� ���� Z   A ������� =  A E��� o   A B���� 0 theepub theEpub� J   B D����  � k   H ��� ��� r   H \��� 6  H Z��� n   H Q��� 2  O Q��
�� 
file� n   H O��� 4   L O���
�� 
cfol� m   M N�� ��� $ P r o d u c t : e B o o k : e P u b� n   H L��� 4   I L���
�� 
cobj� o   J K���� 0 	mainloopx 	mainLoopX� o   H I���� 0 	alltitles 	allTitles� D   R Y��� 1   S U��
�� 
pnam� m   V X�� ���  . z i p� o      ���� 0 theepub theEpub� ���� Z   ] ������� =  ] a��� o   ] ^���� 0 theepub theEpub� J   ^ `����  � k   d q�� ��� r   d i��� m   d e��
�� boovfals� o      ���� 0 everythingcool  � ���� r   j q��� m   j m�� ���  e P u b _ N o t _ F o u n d� o      ���� 0 
theproblem 
theProblem��  ��  � k   t ��� ��� r   t z��� n   t x��� 4   u x���
�� 
cobj� m   v w���� � o   t u���� 0 theepub theEpub� o      ���� 0 theepub theEpub� ���� r   { ���� m   { ~�� ���  z i p� o      ���� 0 	filefound 	fileFound��  ��  ��  � k   � ��� ��� r   � ���� n   � ���� 4   � ����
�� 
cobj� m   � ����� � o   � ����� 0 theepub theEpub� o      ���� 0 theepub theEpub� ���� r   � ���� m   � ��� ���  e p u b� o      ���� 0 	filefound 	fileFound��  ��  ��  ��  � ���� Z   � �� ����� =  � � o   � ����� 0 everythingcool   m   � ���
�� boovtrue  k   � �  l  � ���������  ��  ��    r   � �	 n   � �

 1   � ���
�� 
pnam o   � ����� 0 theepub theEpub	 o      ���� 0 theepubname theEpubName  Z   � ����� =  � � o   � ����� 0 
hasfolders 
hasFolders m   � ���
�� boovtrue Q   � � l  � � I  � �����
�� .coredeloobj        obj  n   � � 2  � ���
�� 
cfol o   � ����� 0 theepubfolder theEpubFolder��   Z T delete unzipped folders which may be left over from running the script previously.     � �   d e l e t e   u n z i p p e d   f o l d e r s   w h i c h   m a y   b e   l e f t   o v e r   f r o m   r u n n i n g   t h e   s c r i p t   p r e v i o u s l y .   R      ������
�� .ascrerr ****      � ****��  ��   k   � �  I  � �����
�� .sysodelanull��� ��� nmbr m   � ����� 
��    ��  I  � ���!��
�� .coredeloobj        obj ! n   � �"#" 2  � ���
�� 
cfol# o   � ����� 0 theepubfolder theEpubFolder��  ��  ��  ��   $%$ l  � ���������  ��  ��  % &��& l  � ���������  ��  ��  ��  ��  ��  ��  w m     ''�                                                                                  MACS  alis    �  32132_TBozanicMBP          �06H+   �!
Finder.app                                                      w��6@%        ����  	                CoreServices    �0Y�      �6xe     �!   !     ;32132_TBozanicMBP:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p  $  3 2 1 3 2 _ T B o z a n i c M B P  &System/Library/CoreServices/Finder.app  / ��  s ()( l     ��������  ��  ��  ) *+* l     ��������  ��  ��  + ,-, l     ��������  ��  ��  - ./. i    010 I      �������� 0 epubcheckit ePubCheckIt��  ��  1 O     �232 k    �44 565 r    	787 c    9:9 o    ���� 0 theepubname theEpubName: m    ��
�� 
TEXT8 o      ���� 0 oldepubname oldEpubName6 ;<; r   
 =>= c   
 ?@? n   
 ABA 7  ��CD
�� 
cha C m    ���� D l   E����E \    FGF l   H����H I   ��I��
�� .corecnte****       ****I o    ���� 0 oldepubname oldEpubName��  ��  ��  G m    ���� ��  ��  B o   
 ���� 0 oldepubname oldEpubName@ m    ��
�� 
TEXT> o      ���� 0 thefilename theFileName< JKJ l     ��������  ��  ��  K LML Z    4NO���N =    #PQP o     !�~�~ 0 
hasfolders 
hasFoldersQ m   ! "�}
�} boovfalsO r   & 0RSR c   & .TUT n   & ,VWV 4   ' ,�|X
�| 
fileX l  ( +Y�{�zY b   ( +Z[Z o   ( )�y�y 0 thefilename theFileName[ m   ) *\\ �]] 
 . e p u b�{  �z  W o   & '�x�x 0 
rootfolder 
rootFolderU m   , -�w
�w 
alisS o      �v�v 0 this_filepath  ��  �  M ^_^ Z   5 \`a�u�t` =  5 8bcb o   5 6�s�s 0 
hasfolders 
hasFoldersc m   6 7�r
�r boovtruea k   ; Xdd efe r   ; Cghg c   ; Aiji n   ; ?klk 4   < ?�qm
�q 
cobjm o   = >�p�p 0 	mainloopx 	mainLoopXl o   ; <�o�o 0 	alltitles 	allTitlesj m   ? @�n
�n 
alish o      �m�m  0 thisisbnfolder thisISBNFolderf n�ln r   D Xopo c   D Vqrq n   D Tsts 4   M T�ku
�k 
fileu l  N Sv�j�iv b   N Swxw o   N O�h�h 0 thefilename theFileNamex m   O Ryy �zz 
 . e p u b�j  �i  t n   D M{|{ 4   H M�g}
�g 
cfol} m   I L~~ � $ P r o d u c t : E b o o k : E p u b| 4   D H�f�
�f 
cfol� o   F G�e�e  0 thisisbnfolder thisISBNFolderr m   T U�d
�d 
alisp o      �c�c 0 this_filepath  �l  �u  �t  _ ��� l  ] ]�b�a�`�b  �a  �`  � ��� r   ] d��� n   ] b��� 1   ^ b�_
�_ 
psxp� o   ] ^�^�^ 0 this_filepath  � o      �]�] 0 this_filepath  � ��� l  e e�\�[�Z�\  �[  �Z  � ��� Q   e ����� k   h ��� ��� l  h ����� I  h ��Y��X
�Y .sysoexecTEXT���     TEXT� b   h }��� b   h y��� b   h s��� b   h o��� m   h k�� ���  j a v a   - j a r  � o   k n�W�W "0 pathtoepubcheck pathToEpubcheck� m   o r�� ���   � n   s x��� 1   t x�V
�V 
strq� o   s t�U�U 0 this_filepath  � m   y |�� ���    2 > & 1  �X  � . (quoted form of prevents ~ from expanding   � ��� P q u o t e d   f o r m   o f   p r e v e n t s   ~   f r o m   e x p a n d i n g� ��� r   � ���� m   � ��� ���  P a s s� o      �T�T 0 	theresult 	theResult� ��S� l  � ��R�Q�P�R  �Q  �P  �S  � R      �O��
�O .ascrerr ****      � ****� l     ��N�M� o      �L�L 0 error_message  �N  �M  � �K��J
�K 
errn� l     ��I�H� o      �G�G 0 error_number  �I  �H  �J  � k   � ��� ��� r   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  E r r o r :  � l  � ���F�E� o   � ��D�D 0 error_number  �F  �E  � m   � ��� ���  .  � l  � ���C�B� o   � ��A�A 0 error_message  �C  �B  � l     ��@�?� o      �>�> 0 
error_text  �@  �?  � ��� Z  � ����=�<� =  � ���� o   � ��;�; 0 
hasfolders 
hasFolders� m   � ��:
�: boovfals� n  � ���� I   � ��9��8�9 0 write_error_log  � ��� l  � ���7�6� o   � ��5�5 0 error_message  �7  �6  � ��4� c   � ���� o   � ��3�3 0 
rootfolder 
rootFolder� m   � ��2
�2 
ctxt�4  �8  �  f   � ��=  �<  � ��� Z  � ����1�0� =  � ���� o   � ��/�/ 0 
hasfolders 
hasFolders� m   � ��.
�. boovtrue� n  � ���� I   � ��-��,�- 0 write_error_log  � ��� l  � ���+�*� o   � ��)�) 0 error_message  �+  �*  � ��(� l  � ���'�&� c   � ���� n   � ���� 4   � ��%�
�% 
cfol� m   � ��� ��� $ P r o d u c t : e B o o k : e P u b� o   � ��$�$  0 thisisbnfolder thisISBNFolder� m   � ��#
�# 
ctxt�'  �&  �(  �,  �  f   � ��1  �0  � ��"� r   � ���� m   � ��� ���  F a i l� o      �!�! 0 	theresult 	theResult�"  � ��� l  � �� ���   �  �  � ��� Z   � ������ =  � ���� o   � ��� 0 	theresult 	theResult� m   � ��� ���  P a s s� l   � �����  ���			
			
			if exists folder "ePubcheck_Passed" of rootFolder then
			else
				make new folder at rootFolder with properties {name:"ePubcheck_Passed"}
			end if
			
			if hasFolders is false then
				set this_filepath to POSIX file this_filepath as alias
				move file this_filepath to folder "ePubcheck_Passed" of rootFolder
			end if
			if hasFolders is true then move thisISBNFolder to folder "ePubcheck_Passed" of rootFolder
			
		end if
		   � ���v 	 	 	 
 	 	 	 
 	 	 	 i f   e x i s t s   f o l d e r   " e P u b c h e c k _ P a s s e d "   o f   r o o t F o l d e r   t h e n 
 	 	 	 e l s e 
 	 	 	 	 m a k e   n e w   f o l d e r   a t   r o o t F o l d e r   w i t h   p r o p e r t i e s   { n a m e : " e P u b c h e c k _ P a s s e d " } 
 	 	 	 e n d   i f 
 	 	 	 
 	 	 	 i f   h a s F o l d e r s   i s   f a l s e   t h e n 
 	 	 	 	 s e t   t h i s _ f i l e p a t h   t o   P O S I X   f i l e   t h i s _ f i l e p a t h   a s   a l i a s 
 	 	 	 	 m o v e   f i l e   t h i s _ f i l e p a t h   t o   f o l d e r   " e P u b c h e c k _ P a s s e d "   o f   r o o t F o l d e r 
 	 	 	 e n d   i f 
 	 	 	 i f   h a s F o l d e r s   i s   t r u e   t h e n   m o v e   t h i s I S B N F o l d e r   t o   f o l d e r   " e P u b c h e c k _ P a s s e d "   o f   r o o t F o l d e r 
 	 	 	 
 	 	 e n d   i f 
 	 	�  � k   � ��� ��� l   � ��� �  �
			if exists folder "ePubcheck_Failed" of rootFolder then
			else
				make new folder at rootFolder with properties {name:"ePubcheck_Failed"}
			end if
			
			
			if hasFolders is false then
				set this_filepath to POSIX file this_filepath as alias
				move this_filepath to folder "ePubcheck_Failed" of rootFolder
				move file (theFileName & "_ePubcheck_error_log.txt") of rootFolder to folder "ePubcheck_Failed" of rootFolder
			end if
			if hasFolders is true then move thisISBNFolder to folder "ePubcheck_Failed" of rootFolder
			     �2 
 	 	 	 i f   e x i s t s   f o l d e r   " e P u b c h e c k _ F a i l e d "   o f   r o o t F o l d e r   t h e n 
 	 	 	 e l s e 
 	 	 	 	 m a k e   n e w   f o l d e r   a t   r o o t F o l d e r   w i t h   p r o p e r t i e s   { n a m e : " e P u b c h e c k _ F a i l e d " } 
 	 	 	 e n d   i f 
 	 	 	 
 	 	 	 
 	 	 	 i f   h a s F o l d e r s   i s   f a l s e   t h e n 
 	 	 	 	 s e t   t h i s _ f i l e p a t h   t o   P O S I X   f i l e   t h i s _ f i l e p a t h   a s   a l i a s 
 	 	 	 	 m o v e   t h i s _ f i l e p a t h   t o   f o l d e r   " e P u b c h e c k _ F a i l e d "   o f   r o o t F o l d e r 
 	 	 	 	 m o v e   f i l e   ( t h e F i l e N a m e   &   " _ e P u b c h e c k _ e r r o r _ l o g . t x t " )   o f   r o o t F o l d e r   t o   f o l d e r   " e P u b c h e c k _ F a i l e d "   o f   r o o t F o l d e r 
 	 	 	 e n d   i f 
 	 	 	 i f   h a s F o l d e r s   i s   t r u e   t h e n   m o v e   t h i s I S B N F o l d e r   t o   f o l d e r   " e P u b c h e c k _ F a i l e d "   o f   r o o t F o l d e r 
 	 	 	�  r   � � m   � ��
� boovfals o      �� 0 everythingcool   � r   � � m   � �		 �

   F a i l e d   e P u b c h e c k o      �� 0 
theproblem 
theProblem�  �  3 m     �                                                                                  MACS  alis    �  32132_TBozanicMBP          �06H+   �!
Finder.app                                                      w��6@%        ����  	                CoreServices    �0Y�      �6xe     �!   !     ;32132_TBozanicMBP:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p  $  3 2 1 3 2 _ T B o z a n i c M B P  &System/Library/CoreServices/Finder.app  / ��  /  l     ����  �  �    i     I      ��� 0 write_error_log    o      �� 0 
this_error   � o      �� 0 wheretowrite whereToWrite�  �   k     N  O     r     b    	 b      o    �� 0 wheretowrite whereToWrite  o    �� 0 thefilename theFileName m    !! �"" 0 _ e P u b c h e c k _ e r r o r _ l o g . t x t l     #�
�	# o      �� 0 	error_log  �
  �	   m     $$�                                                                                  MACS  alis    �  32132_TBozanicMBP          �06H+   �!
Finder.app                                                      w��6@%        ����  	                CoreServices    �0Y�      �6xe     �!   !     ;32132_TBozanicMBP:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p  $  3 2 1 3 2 _ T B o z a n i c M B P  &System/Library/CoreServices/Finder.app  / ��   %�% Q    N&'(& k    2)) *+* I   �,-
� .rdwropenshor       file, 4    �.
� 
file. l   /��/ o    �� 0 	error_log  �  �  - �0� 
� 
perm0 m    ��
�� boovtrue�   + 121 I   )��34
�� .rdwrwritnull���     ****3 l   5����5 b    676 o    ���� 0 
this_error  7 o    ��
�� 
ret ��  ��  4 ��89
�� 
refn8 4    #��:
�� 
file: l  ! ";����; o   ! "���� 0 	error_log  ��  ��  9 ��<��
�� 
wrat< m   $ %��
�� rdwreof ��  2 =��= I  * 2��>��
�� .rdwrclosnull���     ****> 4   * .��?
�� 
file? l  , -@����@ o   , -���� 0 	error_log  ��  ��  ��  ��  ' R      ������
�� .ascrerr ****      � ****��  ��  ( Q   : NAB��A I  = E��C��
�� .rdwrclosnull���     ****C 4   = A��D
�� 
fileD l  ? @E����E o   ? @���� 0 	error_log  ��  ��  ��  B R      ������
�� .ascrerr ****      � ****��  ��  ��  �   FGF l     ��������  ��  ��  G HIH i    "JKJ I      �������� 0 errorhandler errorHandler��  ��  K O     ]LML k    \NN OPO l   ��QR��  Q "  check for "Problems" folder   R �SS 8   c h e c k   f o r   " P r o b l e m s "   f o l d e rP TUT l   ��������  ��  ��  U VWV Z    !X����YX I   ��Z��
�� .coredoexbool        obj Z n    [\[ 4    ��]
�� 
cfol] o    ���� 0 
theproblem 
theProblem\ o    ���� 0 
rootfolder 
rootFolder��  ��  ��  Y I   !����^
�� .corecrel****      � null��  ^ ��_`
�� 
kocl_ m    ��
�� 
cfol` ��ab
�� 
insha o    ���� 0 
rootfolder 
rootFolderb ��c��
�� 
prdtc K    dd ��e��
�� 
pname o    ���� 0 
theproblem 
theProblem��  ��  W fgf l  " "��������  ��  ��  g hih Z   " Fjk����j =  " %lml o   " #���� 0 
theproblem 
theProblemm m   # $nn �oo   F a i l e d   e P u b c h e c kk Z   ( Bpq��rp =  ( +sts o   ( )���� 0 
hasfolders 
hasFolderst m   ) *��
�� boovfalsq I  . 8��uv
�� .coremoveobj        obj u o   . /���� 0 	error_log  v ��w��
�� 
inshw n   0 4xyx 4   1 4��z
�� 
cfolz o   2 3���� 0 
theproblem 
theProblemy o   0 1���� 0 
rootfolder 
rootFolder��  ��  r I  ; B��{|
�� .coremoveobj        obj { o   ; <���� 0 	error_log  | ��}��
�� 
insh} o   = >���� 0 theepubfolder theEpubFolder��  ��  ��  i ~~ I  G Z����
�� .coremoveobj        obj � n   G Q��� 4   J Q���
�� 
cobj� o   M P���� 0 	mainloopx 	mainLoopX� o   G J���� 0 	alltitles 	allTitles� �����
�� 
insh� n   R V��� 4   S V���
�� 
cfol� o   T U���� 0 
theproblem 
theProblem� o   R S���� 0 
rootfolder 
rootFolder��   ���� l  [ [��������  ��  ��  ��  M m     ���                                                                                  MACS  alis    �  32132_TBozanicMBP          �06H+   �!
Finder.app                                                      w��6@%        ����  	                CoreServices    �0Y�      �6xe     �!   !     ;32132_TBozanicMBP:System: Library: CoreServices: Finder.app    
 F i n d e r . a p p  $  3 2 1 3 2 _ T B o z a n i c M B P  &System/Library/CoreServices/Finder.app  / ��  I ��� l     ��������  ��  ��  � ��� i   # &��� I      �������� 0 fanfare  ��  ��  � k     �� ��� O    
��� I   	������
�� .miscactvnull��� ��� obj ��  ��  �  f     � ���� I   ����
�� .sysodlogaskr        TEXT� b    ��� b    ��� b    ��� b    ��� m    �� ��� 6 B a t c h   e P u b c h e c k   c o m p l e t e . 
 
� o    ���� 0 successcount successCount� m    �� ��� @   t i t l e s   s u c c e s s f u l l y   p r o c e s s e d . 
� o    ���� 0 problemcount problemCount� m    �� ���     p r o b l e m   t i t l e s .� �����
�� 
btns� J    �� ���� m    �� ���  D o n e��  ��  ��  � ��� l     ��������  ��  ��  � ���� l     ��������  ��  ��  ��       *��� ������������ �����������������	������~�}�|�{�z�y�x�w�v�u�t�s��  � (�r�q�p�o�n�m�l�k�j�i�h�g�f�e�d�c�b�a�`�_�^�]�\�[�Z�Y�X�W�V�U�T�S�R�Q�P�O�N�M�L�K�r 20 currentepubcheckversion currentEpubcheckVersion
�q .aevtodocnull  �    alis
�p .aevtoappnull  �   � ****�o 0 main  �n 0 
getfolders 
getFolders�m 0 getepub getEpub�l 0 epubcheckit ePubCheckIt�k 0 write_error_log  �j 0 errorhandler errorHandler�i 0 fanfare  �h 0 thisitem  �g 0 dragdrop  �f "0 pathtoepubcheck pathToEpubcheck�e 0 successcount successCount�d 0 problemcount problemCount�c 0 
rootfolder 
rootFolder�b 0 
hasfolders 
hasFolders�a 0 	alltitles 	allTitles�` 0 	mainloopx 	mainLoopX�_ 0 everythingcool  �^ 0 theepub theEpub�] 0 theepubname theEpubName�\ 0 thefilename theFileName�[ 0 	error_log  �Z 0 
theproblem 
theProblem�Y  �X  �W  �V  �U  �T  �S  �R  �Q  �P  �O  �N  �M  �L  �K  � �J ]�I�H���G
�J .aevtodocnull  �    alis�I 0 theitems theItems�H  � �F�F 0 theitems theItems� �E�D�C�E 0 thisitem  �D 0 dragdrop  �C 0 main  �G �E�OeE�O*j+ � �B n�A�@���?
�B .aevtoappnull  �   � ****�A  �@  �  � �>�=�> 0 dragdrop  �= 0 main  �? 
fE�O*j+ � �< z�;�:���9�< 0 main  �;  �:  �  �  ��8�7�6�5�4�3�2�1�0�/�.�-�,
�8 
ctxt�7 "0 pathtoepubcheck pathToEpubcheck�6 0 successcount successCount�5 0 problemcount problemCount�4 0 
getfolders 
getFolders�3 0 	mainloopx 	mainLoopX�2 0 	alltitles 	allTitles
�1 .corecnte****       ****�0 0 everythingcool  �/ 0 getepub getEpub�. 0 epubcheckit ePubCheckIt�- 0 errorhandler errorHandler�, 0 fanfare  �9 ��&E�OjE�OjE�O*j+ OkE�O _h��j eE�O*j+ 
O�e  
*j+ Y hO�f  
*j+ Y hO�kE�O�e  
�kE�Y hO�f  
�kE�Y h[OY��O*j+ � �+ ��*�)���(�+ 0 
getfolders 
getFolders�*  �)  � �'�&�%�' 0 foldertested  �& 0 folder_test_1  �% 0 folder_test_2  � n�$�#�" ��!� ���������Hc��
�$ .miscactvnull��� ��� obj �# 0 dragdrop  
�" 
prmp
�! .sysostflalis    ��� null�  0 
rootfolder 
rootFolder� 0 thisitem  
� 
alis
� 
cfol
� 
file�  
� 
pnam
� .corecnte****       ****� 0 
hasfolders 
hasFolders� 0 	alltitles 	allTitles
� 
disp
� .sysodlogaskr        TEXT�( �� �*j OfE�O �h�e �f  *��l E�Y ��&E�O*��/�-�[�,\Z�?1E�O�j j fE�O�E` OeE�Y hO�j j  =��-�[�,\Za >1E�O�j j eE�O�E` OeE�Y a a jl OPY h[OY�mOPU� �u������ 0 getepub getEpub�  �  � ��� 0 	filefound 	fileFound�  0 thisfoldername thisFolderName� '�������
�	����������������� ��� 0 
hasfolders 
hasFolders� 0 	alltitles 	allTitles
� 
cobj� 0 	mainloopx 	mainLoopX� 0 theepub theEpub
�
 
pnam
�	 
cfol� 0 theepubfolder theEpubFolder
� 
file� 0 everythingcool  � 0 
theproblem 
theProblem� 0 theepubname theEpubName
� .coredeloobj        obj �  �  �  

�� .sysodelanull��� ��� nmbr� �� ��f  ���/E�O�E�Y hO�e  u���/�,E�O���/��/E�O��-�[�,\Z�?1E�O�jv  =���/��/�-�[�,\Z�?1E�O�jv  fE` Oa E` Y ��k/E�Oa E�Y ��k/E�Oa E�Y hO_ e  :��,E` O�e  & ��-j W X  a j O��-j Y hOPY hU� ��1���������� 0 epubcheckit ePubCheckIt��  ��  � ���������������� 0 oldepubname oldEpubName�� 0 this_filepath  ��  0 thisisbnfolder thisISBNFolder�� 0 	theresult 	theResult�� 0 error_message  �� 0 error_number  �� 0 
error_text  � &������������������\����������~y��������������������������	���� 0 theepubname theEpubName
�� 
TEXT
�� 
cha 
�� .corecnte****       ****�� �� 0 thefilename theFileName�� 0 
hasfolders 
hasFolders�� 0 
rootfolder 
rootFolder
�� 
file
�� 
alis�� 0 	alltitles 	allTitles
�� 
cobj�� 0 	mainloopx 	mainLoopX
�� 
cfol
�� 
psxp�� "0 pathtoepubcheck pathToEpubcheck
�� 
strq
�� .sysoexecTEXT���     TEXT�� 0 error_message  � ������
�� 
errn�� 0 error_number  ��  
�� 
ctxt�� 0 write_error_log  �� 0 everythingcool  �� 0 
theproblem 
theProblem�� �� ���&E�O�[�\[Zk\Z�j �2�&E�O�f  ����%/�&E�Y hO�e  "���/�&E�O*�/�a /��a %/�&E�Y hO�a ,E�O &a _ %a %�a ,%a %j Oa E�OPW KX  a �%a %�%E�O�f  )��a &l+ Y hO�e  )���a  /a &l+ Y hOa !E�O�a "  hY fE` #Oa $E` %U� ������������ 0 write_error_log  �� ����� �  ������ 0 
this_error  �� 0 wheretowrite whereToWrite��  � ������ 0 
this_error  �� 0 wheretowrite whereToWrite� $��!���������������������������� 0 thefilename theFileName�� 0 	error_log  
�� 
file
�� 
perm
�� .rdwropenshor       file
�� 
ret 
�� 
refn
�� 
wrat
�� rdwreof �� 
�� .rdwrwritnull���     ****
�� .rdwrclosnull���     ****��  ��  �� O� 	��%�%E�UO '*��/�el O��%�*��/��� O*��/j W X   *��/j W X  h� ��K���������� 0 errorhandler errorHandler��  ��  �  � ���������������������n���������������� 0 
rootfolder 
rootFolder
�� 
cfol�� 0 
theproblem 
theProblem
�� .coredoexbool        obj 
�� 
kocl
�� 
insh
�� 
prdt
�� 
pnam�� 
�� .corecrel****      � null�� 0 
hasfolders 
hasFolders�� 0 	error_log  
�� .coremoveobj        obj �� 0 theepubfolder theEpubFolder�� 0 	alltitles 	allTitles
�� 
cobj�� 0 	mainloopx 	mainLoopX�� ^� Z���/j  hY *�������l� 
O��  �f  �����/l Y 	���l Y hO_ a _ /����/l OPU� ������������� 0 fanfare  ��  ��  �  � 	��������������
�� .miscactvnull��� ��� obj �� 0 successcount successCount�� 0 problemcount problemCount
�� 
btns
�� .sysodlogaskr        TEXT�� ) *j  UO��%�%�%�%��kvl � ����� �  ��,bmrkbook(    0                                   �          �     Users        tbozanicmbp      Desktop      tmp      tech     tools2       distroValidate       errorExamples   	     epubCheck        Failure (            4   D   P   \   l   �   �   �        Bp          d          t          ٙ          i�          .�/         $�z         H}         Y}         -ѽ    (     �           0  @  P  `  p  �        A�x|�                                               �     	  file:///     32132_TBozanicMBP         @M��         A����   $     9BC3870C-9C61-3CDD-BBC5-05313BA205BA     �     �                 /              ���o_�A0     dnib                               rdlf       �     ec803a9b10b06db914838f61846d6ae7476377b1;00000000;00000000;0000000000000020;com.apple.app-sandbox.read-write;00000001;01000002;0000000001bdd12d;/users/tbozanicmbp/desktop/tmp/tech/tools2/distrovalidate/errorexamples/epubcheck/failure   �   ����            �         �        �      @  �         �                           X         8         H          �      0   �      �  �      �          �         �         �  �       !�  �      0�  �      ��         
�� boovfals��  ��  �alis      32132_TBozanicMBP          �06H+  }YFailure                                                        ��-���s        ����  	                	epubCheck     �0Y�      ����    $}Y}Hz�$/�. �i �� t d pB  s32132_TBozanicMBP:Users: tbozanicmbp: Desktop: tmp: tech: tools2: distroValidate: errorExamples: epubCheck: Failure     F a i l u r e  $  3 2 1 3 2 _ T B o z a n i c M B P  XUsers/tbozanicmbp/Desktop/tmp/tech/tools2/distroValidate/errorExamples/epubCheck/Failure  /    ��  
�� boovfals� ����� �  ���� �� ����� ����� ����� ����� ����� ����� ����� ����� ����� ����� ����� n��
�� 
sdsk
�� 
cfol� ��� 
 U s e r s
�� 
cfol� ���  t b o z a n i c m b p
�� 
cfol� ���  D e s k t o p
�� 
cfol� ���  t m p
�� 
cfol� ���  t e c h
�� 
cfol� ���  t o o l s 2
�� 
cfol� ���  d i s t r o V a l i d a t e
�� 
cfol� ���  e r r o r E x a m p l e s
�� 
cfol� ���  e p u b C h e c k
�� 
cfol� ���  F a i l u r e
�� 
docf� ��� F B e r e _ 9 7 8 1 4 9 6 7 0 3 3 9 2 _ e p u b _ a l l _ r 1 . e p u b� �� ����� ��� � �� �� �� �� 	��
	 �� �� �� �� n��
�� 
sdsk
�� 
cfol � 
 U s e r s
�� 
cfol �  t b o z a n i c m b p
�� 
cfol �  D e s k t o p
�� 
cfol �  t m p
�� 
cfol
 �  t e c h
�� 
cfol �  t o o l s 2
�� 
cfol �  d i s t r o V a l i d a t e
�� 
cfol �  e r r o r E x a m p l e s
�� 
cfol �  e p u b C h e c k
�� 
cfol  �  F a i l u r e
�� 
docf� � D C o x _ 9 7 8 1 6 1 6 5 0 6 0 6 3 _ e p u b _ a l l _ r 1 . e p u b�  ��  !��"! #��$# %��&% '��(' )��*) +��,+ -��.- /��0/ 1��21 3��43 n�
� 
sdsk
�� 
cfol4 �55 
 U s e r s
�� 
cfol2 �66  t b o z a n i c m b p
�� 
cfol0 �77  D e s k t o p
�� 
cfol. �88  t m p
�� 
cfol, �99  t e c h
�� 
cfol* �::  t o o l s 2
�� 
cfol( �;;  d i s t r o V a l i d a t e
�� 
cfol& �<<  e r r o r E x a m p l e s
�� 
cfol$ �==  e p u b C h e c k
�� 
cfol" �>>  F a i l u r e
�� 
docf  �?? F D a d e _ 9 7 8 1 6 0 1 8 3 7 9 7 4 _ e p u b _ a l l _ r 1 . e p u b�� 
�� boovtrue� �@@ F D a d e _ 9 7 8 1 6 0 1 8 3 7 9 7 4 _ e p u b _ a l l _ r 1 . e p u b� �AA < D a d e _ 9 7 8 1 6 0 1 8 3 7 9 7 4 _ e p u b _ a l l _ r 1� �BB@ 3 2 1 3 2 _ T B o z a n i c M B P : U s e r s : t b o z a n i c m b p : D e s k t o p : t m p : t e c h : t o o l s 2 : d i s t r o V a l i d a t e : e r r o r E x a m p l e s : e p u b C h e c k : F a i l u r e : C o x _ 9 7 8 1 6 1 6 5 0 6 0 6 3 _ e p u b _ a l l _ r 1 _ e P u b c h e c k _ e r r o r _ l o g . t x t��  ��  �  �~  �}  �|  �{  �z  �y  �x  �w  �v  �u  �t  �s   ascr  ��ޭ